<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! function_exists( 'neve_child_load_css' ) ):
	/**
	 * Load CSS file.
	 */
	function neve_child_load_css() {
		wp_enqueue_style( 'neve-child-style', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'neve-style' ), NEVE_VERSION );
	}
endif;
add_action( 'wp_enqueue_scripts', 'neve_child_load_css', 20 );

/* ---------- Remove core patterns ---------- */
remove_theme_support( 'core-block-patterns' );
unregister_block_pattern( 'core/dark-header-with-centered-content' );
unregister_block_pattern( 'core/heading-paragraph' );
unregister_block_pattern( 'core/large-header-button' );
unregister_block_pattern( 'core/quote' );
unregister_block_pattern( 'core/text-three-columns-buttons' );
unregister_block_pattern( 'core/text-two-columns-with-images' );
unregister_block_pattern( 'core/text-two-columns' );
unregister_block_pattern( 'core/text-two-columns-with-text' );
unregister_block_pattern( 'core/three-buttons' );
unregister_block_pattern( 'core/two-buttons' );
unregister_block_pattern( 'core/two-images' );
unregister_block_pattern( 'neve/light-header-left-aligned-content' );


/* 360online base functionality */

/* Add header and description to product page */

add_action('woocommerce_archive_description', 'show_category_title', 10, 2);

function show_category_title() {
	$cat_title = single_tag_title("", false);
	echo '<h1>' . $cat_title . '</h1>';
}

add_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 100 );
function woocommerce_taxonomy_archive_description() {
if ( is_product_category()) {
$cat_desc = term_description( $cat_id, 'product_cat' );
echo $cat_desc;
}
}

/* */ 
/**
 * @snippet       Add new textarea to Product Category Pages - WooCommerce
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.9
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */  
 
// ---------------
// 1. Display field on "Add new product category" admin page
 
add_action( 'product_cat_add_form_fields', 'bbloomer_wp_editor_add', 10, 2 );
 
function bbloomer_wp_editor_add() {
    ?>
    <div class="form-field">
        <label for="seconddesc"><?php echo __( 'Second Description', 'woocommerce' ); ?></label>
       
      <?php
      $settings = array(
         'textarea_name' => 'seconddesc',
         'quicktags' => array( 'buttons' => 'em,strong,link' ),
         'tinymce' => array(
            'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
            'theme_advanced_buttons2' => '',
         ),
         'editor_css' => '<style>#wp-excerpt-editor-container .wp-editor-area{height:175px; width:100%;}</style>',
      );
 
      wp_editor( '', 'seconddesc', $settings );
      ?>
       
        <p class="description"><?php echo __( 'This is the description that goes BELOW products on the category page', 'woocommerce' ); ?></p>
    </div>
    <?php
}
 
// ---------------
// 2. Display field on "Edit product category" admin page
 
add_action( 'product_cat_edit_form_fields', 'bbloomer_wp_editor_edit', 10, 2 );
 
function bbloomer_wp_editor_edit( $term ) {
    $second_desc = htmlspecialchars_decode( get_woocommerce_term_meta( $term->term_id, 'seconddesc', true ) );
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="second-desc"><?php echo __( 'Second Description', 'woocommerce' ); ?></label></th>
        <td>
            <?php
          
         $settings = array(
            'textarea_name' => 'seconddesc',
            'quicktags' => array( 'buttons' => 'em,strong,link' ),
            'tinymce' => array(
               'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
               'theme_advanced_buttons2' => '',
            ),
            'editor_css' => '<style>#wp-excerpt-editor-container .wp-editor-area{height:175px; width:100%;}</style>',
         );
 
         wp_editor( $second_desc, 'seconddesc', $settings );
         ?>
       
            <p class="description"><?php echo __( 'This is the description that goes BELOW products on the category page', 'woocommerce' ); ?></p>
        </td>
    </tr>
    <?php
}
 
// ---------------
// 3. Save field @ admin page
 
add_action( 'edit_term', 'bbloomer_save_wp_editor', 10, 3 );
add_action( 'created_term', 'bbloomer_save_wp_editor', 10, 3 );
 
function bbloomer_save_wp_editor( $term_id, $tt_id = '', $taxonomy = '' ) {
   if ( isset( $_POST['seconddesc'] ) && 'product_cat' === $taxonomy ) {
      update_woocommerce_term_meta( $term_id, 'seconddesc', esc_attr( $_POST['seconddesc'] ) );
   }
}
 
// ---------------
// 4. Display field under products @ Product Category pages 
 
add_action( 'neve_before_footer_hook', 'bbloomer_display_wp_editor_content', 1000 );
 
function bbloomer_display_wp_editor_content() {
   if ( is_product_taxonomy() ) {
      $term = get_queried_object();
      if ( $term && ! empty( get_woocommerce_term_meta( $term->term_id, 'seconddesc', true ) ) ) {
         echo '<div class="woo-second-description-wrapper"><div class="woo-second-description">' . wc_format_content( htmlspecialchars_decode( get_woocommerce_term_meta( $term->term_id, 'seconddesc', true ) ) ) . '</div></div>';
      }
   }
}

/**
 * Change the breadcrumb separator
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter', 20 );
function wcc_change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = '<span class="material-icons">chevron_right</span>';
	return $defaults;
}

// remove the subcategories from the product loop
remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );

// add subcategories before the product loop (yet after catalog_ordering and result_count -> see priority 40)
add_action( 'woocommerce_before_shop_loop', 'wp56123_show_product_subcategories', 40 );

function wp56123_show_product_subcategories() {
    $subcategories = woocommerce_maybe_show_product_subcategories();
        if ($subcategories) {
          echo '<ul class="subcategories">',$subcategories,'</ul>';
          echo '<h3 class="product-heading">Produkter</h3>';
    }
}

/**
 * Material icons
 */
function add_custom_font() { 
	?>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet">
	<?php
}
add_action( 'wp_head', 'add_custom_font' );
add_action( 'customize_controls_print_styles', 'add_custom_font' );


function add_custom_fonts( $localized_data ) {
	$localized_data['fonts']['Custom'][] = 'MaterialIcons';
	return $localized_data;
}
add_filter( 'neve_react_controls_localization', 'add_custom_fonts' );

/* -------------------------- Custom -------------------------- */



/* -------------------------- Custom -------------------------- */

function change_blog_image_size() {
	remove_image_size( 'neve-blog' );
	add_image_size( 'neve-blog', 300, 300, true );
}
add_action( 'after_setup_theme', 'change_blog_image_size', 100 );

/* CUSTOM FONTS */
